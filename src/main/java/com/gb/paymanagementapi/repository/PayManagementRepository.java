package com.gb.paymanagementapi.repository;

import com.gb.paymanagementapi.entity.PayManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayManagementRepository extends JpaRepository<PayManagement, Long> {
}
