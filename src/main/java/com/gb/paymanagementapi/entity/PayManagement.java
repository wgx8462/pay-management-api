package com.gb.paymanagementapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PayManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String name;
    @Column(nullable = false)
    private Double basicPay;
    @Column(nullable = false)
    private Double nationalPension;
    @Column(nullable = false)
    private Double employmentIns;
    @Column(nullable = false)
    private Double HealthIns;
    @Column(nullable = false)
    private Double longCareIns;
    @Column(nullable = false)
    private Double incomeTax;
    @Column(nullable = false)
    private Double taxFree;
}
