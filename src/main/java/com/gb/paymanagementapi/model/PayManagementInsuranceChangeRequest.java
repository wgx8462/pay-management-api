package com.gb.paymanagementapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayManagementInsuranceChangeRequest {
    private Double nationalPension;
    private Double employmentIns;
    private Double HealthIns;
    private Double longCareIns;
    private Double incomeTax;
}
