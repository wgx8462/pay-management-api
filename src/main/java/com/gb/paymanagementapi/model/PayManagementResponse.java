package com.gb.paymanagementapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayManagementResponse {
    private Long id;
    private String name;
    private Double basicPay;
    private Double nationalPension;
    private Double employmentIns;
    private Double HealthIns;
    private Double longCareIns;
    private Double incomeTax;
    private Double taxFree;
    private Double afterTax;
}
