package com.gb.paymanagementapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayManagementBasicPayChangeRequest {
    private Double basicPay;
}
