package com.gb.paymanagementapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayManagementItem {
    private Long id;
    private String name;
    private Double basicPay;
}
