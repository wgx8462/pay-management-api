package com.gb.paymanagementapi.controller;

import com.gb.paymanagementapi.model.*;
import com.gb.paymanagementapi.service.PayManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pay-management")
public class PayManagementController {
    private final PayManagementService payManagementService;

    @PostMapping("/new")
    public String setPayManagement(@RequestBody PayManagementRequest request) {
        payManagementService.setPayManagement(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PayManagementItem> getPayManagements() {
        return payManagementService.getPayManagements();
    }

    @GetMapping("/detail/{id}")
    public PayManagementResponse getPayManagement(@PathVariable long id) {
        return payManagementService.getPayManagement(id);
    }

    @PutMapping("/basic-pay/{id}")
    public String putPayManagementBasic(@PathVariable long id, @RequestBody PayManagementBasicPayChangeRequest request) {
        payManagementService.putPayManagementBasic(id, request);

        return "OK";
    }

    @PutMapping("/insurance/{id}")
    public String putPayManagementInsurance(@PathVariable long id, @RequestBody PayManagementInsuranceChangeRequest request) {
        payManagementService.putPayManagementInsurance(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delPayManagement(@PathVariable long id) {
        payManagementService.delPayManagement(id);

        return "Ok";
    }
}
