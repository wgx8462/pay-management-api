package com.gb.paymanagementapi.service;

import com.gb.paymanagementapi.entity.PayManagement;
import com.gb.paymanagementapi.model.*;
import com.gb.paymanagementapi.repository.PayManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PayManagementService {
    private final PayManagementRepository payManagementRepository;

    public void setPayManagement(PayManagementRequest request) {
        PayManagement addData = new PayManagement();
        addData.setName(request.getName());
        addData.setBasicPay(request.getBasicPay());
        addData.setNationalPension(request.getNationalPension());
        addData.setEmploymentIns(request.getEmploymentIns());
        addData.setHealthIns(request.getHealthIns());
        addData.setLongCareIns(request.getLongCareIns());
        addData.setIncomeTax(request.getIncomeTax());
        addData.setTaxFree(request.getTaxFree());

        payManagementRepository.save(addData);
    }

    public List<PayManagementItem> getPayManagements() {
        List<PayManagement> originList = payManagementRepository.findAll();

        List<PayManagementItem> result = new LinkedList<>();

        for (PayManagement payManagement : originList) {
            PayManagementItem addItem = new PayManagementItem();
            addItem.setId(payManagement.getId());
            addItem.setName(payManagement.getName());
            addItem.setBasicPay(payManagement.getBasicPay());

            result.add(addItem);
        }
        return result;
    }

    public PayManagementResponse getPayManagement(long id) {
        PayManagement originList = payManagementRepository.findById(id).orElseThrow();

        PayManagementResponse response = new PayManagementResponse();
        response.setId(originList.getId());
        response.setName(originList.getName());
        response.setBasicPay(originList.getBasicPay());
        response.setNationalPension(originList.getNationalPension());
        response.setEmploymentIns(originList.getEmploymentIns());
        response.setHealthIns(originList.getHealthIns());
        response.setLongCareIns(originList.getLongCareIns());
        response.setIncomeTax(originList.getIncomeTax());
        response.setTaxFree(originList.getTaxFree());
        response.setAfterTax(originList.getBasicPay() - originList.getNationalPension() - originList.getEmploymentIns() - originList.getHealthIns() - originList.getLongCareIns() - originList.getIncomeTax());

        return response;
    }

    public void putPayManagementBasic(long id, PayManagementBasicPayChangeRequest request) {
        PayManagement originData = payManagementRepository.findById(id).orElseThrow();
        originData.setBasicPay(request.getBasicPay());

        payManagementRepository.save(originData);
    }

    public void putPayManagementInsurance(long id, PayManagementInsuranceChangeRequest request) {
        PayManagement originData = payManagementRepository.findById(id).orElseThrow();
        originData.setNationalPension(request.getNationalPension());
        originData.setEmploymentIns(request.getEmploymentIns());
        originData.setHealthIns(request.getHealthIns());
        originData.setLongCareIns(request.getLongCareIns());
        originData.setIncomeTax(request.getIncomeTax());

        payManagementRepository.save(originData);
    }

    public void delPayManagement(long id) {
        payManagementRepository.deleteById(id);
    }
}
