package com.gb.paymanagementapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayManagementApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayManagementApiApplication.class, args);
	}

}
